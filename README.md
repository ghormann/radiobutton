# README #

This is a small python script that runs on Pi2.  Each time the button is pressed the radio os turned on for a few minutes. 
Button presses are traced on a remte server. The light on the button flashes unless the timmer is being pressed. 

### Configuration ###
* pip install paho-mqtt

### Usage ###
* python radiobutton.py   # Monitor for Pi
* mosquitto_pub -t 'radiobutton/1/override' -m "1"    # Set Override
