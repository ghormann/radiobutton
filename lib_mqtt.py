import paho.mqtt.client as mqtt
import time

class RadioMqtt: 

    def __init__(self, radio_button):
        self.radio_button = radio_button 
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.connect("192.168.0.212", 1883, 60)
        self.client.loop_start()

    def on_connect(self,client, userdata, flags, rc):
        print("Connected with result code " + str(rc))

        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        client.subscribe("radiobutton/1/#", 2)
        client.publish("test", "Publish is complete", 2)

    # The callback for when a PUBLISH message is received from the server.


    def publish_button(self):
        self.client.publish("radiobutton/1/button", "1", 2)

    def publish_radio_status(self, status):
        msg = 0
        if status: 
            msg = 1
        self.client.publish("radiobutton/1/radio", msg, 2)

    
    def on_message(self, client, userdata, msg):
        print("DEBUG: " + msg.topic + " " + str(msg.payload))
        if msg.topic == "radiobutton/1/override":
            if "1" == msg.payload:
                self.radio_button.set_override(True);
            else:
                self.radio_button.set_override(False);
        else:
            print(msg.topic + " " + str(msg.payload))