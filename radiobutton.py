"""
THis program is for the radio button out in the yard.
It will play the radio (Relay) for a given period of time
After the button is pressed
"""
import datetime
import logging
import time

import lib_mqtt as mqtt
import RPi.GPIO as GPIO

logger = logging.getLogger(__name__)


class RadioButton(object):
    """
    This program is for the radio button out in the yard.
    It will play the radio (Relay) for a given period of time
    After the button is pressed
    """
    PIN_LED = 7
    PIN_RELAY = 8
    PIN_SWITCH = 11
    RADIO_DURATION = 5  # Duration radio is on (Seconds)
    radio_ts = datetime.datetime.now()
    radio_on = False
    radio_override = False
    led_on = False

    def __init__(self):
        """ Initialize GPIO Pins"""
        def handle_button(channel):
            print "Button: " + str(channel)
            self.mqtt.publish_button()
            self.start_radio()

        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.PIN_LED, GPIO.OUT)
        GPIO.setup(self.PIN_RELAY, GPIO.OUT)
        GPIO.setup(self.PIN_SWITCH, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.output(self.PIN_LED, GPIO.LOW)
        GPIO.output(self.PIN_RELAY, GPIO.LOW)
        GPIO.add_event_detect(self.PIN_SWITCH, GPIO.RISING,
                              callback=handle_button, bouncetime=200)
        self.mqtt = mqtt.RadioMqtt(self)

    def toggle_led(self):
        """Toggles The LED each time it is called """
        if self.led_on:
            self.led_on = False
            GPIO.output(self.PIN_LED, GPIO.LOW)
        else:
            self.led_on = True
            GPIO.output(self.PIN_LED, GPIO.HIGH)

    def stop_radio(self):
        """ Stop paying the Radio """
        logger.debug('Stopping Radio')
        GPIO.output(self.PIN_RELAY, GPIO.LOW)
        self.radio_on = False
        self.led_on = False
        self.toggle_led()
        self.mqtt.publish_radio_status(False)

    def set_override(self, boolVal):
        self.radio_override = boolVal
        if boolVal:
            self.start_radio()
        else:
            self.stop_radio()
    
    def start_radio(self):
        """ Start paying the Radio """
        logger.debug('Starting Radio')
        GPIO.output(self.PIN_RELAY, GPIO.HIGH)
        GPIO.output(self.PIN_LED, GPIO.LOW)
        self.radio_ts = datetime.datetime.now()
        if not self.radio_on:
            self.radio_on = True
            self.mqtt.publish_radio_status(True)

    def run(self):
        """This is the main function.  It will never return"""
        try:
            while 1:
                now = datetime.datetime.now()
                time.sleep(0.5)
                if self.radio_override:
                    self.start_radio()
                elif now.hour < 17 or now.hour > 22:
                    logger.debug('Force off because of hour')
                    GPIO.output(self.PIN_LED, GPIO.LOW)
                    self.stop_radio()
                    time.sleep(10)
                elif self.radio_on:
                    time_diff = (datetime.datetime.now() -
                                 self.radio_ts).total_seconds()
                    print "Time_diff %d, radio_on: %d" % (time_diff, self.radio_on)
                    if time_diff > self.RADIO_DURATION:
                        self.stop_radio()
                    else:
                        GPIO.output(self.PIN_LED, GPIO.LOW)
                else:  # Not override and Radio off
                    self.toggle_led()

        finally:  # Control-C
            print "Exiting and cleaning up"
            GPIO.cleanup()


if __name__ == "__main__":
    r = RadioButton()
    r.run()
